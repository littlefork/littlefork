
# Littlefork toolkit

The Littlefork toolkit consists of [Littlefork Core](https://gitlab.com/littlefork/littlefork), the [Littlefork command line interface](https://gitlab.com/littlefork/littlefork-cli) and a set of curated plugins.

The toolkit is available as NPM package. The repository URL is https://gitlab.com/littlefork/littlefork-toolkit.
