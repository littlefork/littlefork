import {flow, forEach} from 'lodash/fp';
import {utils} from 'littlefork-core';
import shell from 'shelljs';
import modules from '../lib/modules';

const {mkdirP} = utils.fs;

const copyModuleReferences = flow([
  modules,
  forEach(module => {
    const source = `node_modules/${module}/R*.md`;
    const target = `docs/modules/${module.replace('littlefork-plugin-', '')}.md`;
    shell.cp(source, target);
  }),
]);

const copyApiReference = () =>
  shell.cp('node_modules/littlefork-core/docs/api.md', 'docs/api.md');

mkdirP('docs/modules')
  .then(() => {
    copyModuleReferences();
    copyApiReference();
  });
