# littlefork-plugin-csv

Convert littlefork data from and to CSV.

## Plugins

- `csv_export`

Export littlefork data in CSV format to a file.

### Options

- `csv.delimiter` (defaults to `,`)

  Specify the CSV delimiter.

- `csv.filename` (defaults to `out.csv`). Specify the target file.

`littlefork -c config.json -p google_search,csv_export --csv.filename data.csv`

- `csv_import`

Import a CSV file, and turn it into littlefork data.

### Options

Uses `glob_pattern` as query type.

- `csv.delimiter` (defaults to `,`)

  Specify the CSV delimiter.

- `csv.id_fields` (required). Specify one or several field names (separated by
  a comma), that are used to determine the identity of a record

`littlefork -Q glob_pattern:data/**/*.csv -p csv_import,tap_printf --csv.id_fields firstName,lastName`
