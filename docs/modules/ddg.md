# littlefork-plugin-ddg

This is a plugin for [littlefork](https://gitlab.com/littlefork/littlefork).

Search on DuckDuckGo.

## Installation

```
npm install --save littlefork-plugin-ddg
```

## Usage

This plugin exports a single transformation plugin:

### `ddg_search` transformation

```
$(npm bin)/littlefork -c cfg.json -p ddg_search
```

This is a data fetching plugin.

It looks for the query terms of type `ddg_search`, e.g.:

```
[{
  "type": "ddg_search",
  "term": "Some Search Term"
}]
```
