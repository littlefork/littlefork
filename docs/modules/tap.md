# littlefork-plugin-tap

This is a module for [littlefork](https://gitlab.com/littlefork/littlefork).

## Installation

```
npm install --save littlefork-plugin-tap
```

## Usage

### `tap_printf` plugin

```
$(npm bin)/littlefork -c cfg.json -p twitter_feed,tap_printf
```

Prints the whole envelope (i.e. data and queries) and the full configuration
to the screen.

**Configuration**:

- `tap.limit` :: Limit the output to <n> data units.

   Example: Print only 5 units of data.

   `$(npm bin)/littlefork -c cfg.json -p twitter_feed,tap_printf --tap.limit 5`

### `tap_writef` plugin

```
$(npm bin)/littlefork -c cfg.json -p twitter_feed,tap_writef
```

Write the envelope to data to a file.

**Configuration**:

- `tap.limit` :: Limit the output to <n> data units.

   Example: Print only 5 units of data.

   `$(npm bin)/littlefork -c cfg.json -p twitter_feed,tap_writef --tap.limit 5`

- `tap.filename` :: Specify the name of the output file. Defaults to
  `data-<marker>.json`.
